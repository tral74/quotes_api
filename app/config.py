import os
from enum import Enum

from pydantic import BaseSettings, AnyUrl, PositiveInt


class LoadBalancerModes(str, Enum):
    requests_number = 'requests_number'
    last_request_time = 'last_request_time'


class Settings(BaseSettings):
    env = os.getenv("ENVIRONMENT", "dev")

    if env == 'dev':
        REDIS_URL: AnyUrl = "redis://localhost"

    elif env == 'stage':
        REDIS_URL: AnyUrl = "redis://localhost"

    elif env == 'prod':
        REDIS_URL: AnyUrl = "redis://host.docker.internal"

    REDIS_POOL_SIZE: int = 10

    PROVIDERS: dict = {
        'frankfurter': {
            'title': 'Frankfurter',
            'url_template': 'https://api.frankfurter.app/latest?from=%s'
        },
        'exchange_rate_api': {
            'title': 'ExchangeRate',
            'url_template': 'https://api.exchangerate-api.com/v4/latest/%s'
        }
    }

    LOAD_BALANCER_MODE: LoadBalancerModes = 'last_request_time'

    MAX_DECIMAL_PLACES_IN_RATE: int = 3

    PROVIDERS_KEY: str = 'providers'

    CACHE_ENABLED: bool = True

    CACHE_KEY_PREFIX: str = 'RATE'

    TTL_IN_MINUTES: PositiveInt = 1


settings = Settings()
