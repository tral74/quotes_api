from enum import Enum

import pycountry
from pydantic import BaseModel

# Enum for checking currency codes
Currencies = Enum(
    "Currencies",
    {r.alpha_3: r.alpha_3 for r in pycountry.currencies}
)


class QuoteResponse(BaseModel):
    exchange_rate: float
    currency_code: Currencies
    amount: int
    provider_name: str
