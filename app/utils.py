import math
from typing import Union

from app.common import redis_db
from app.config import settings


def truncate(val: Union[int, float], decimal_places: int = 3) -> Union[int, float]:
    factor = 10.0 ** decimal_places
    return math.trunc(val * factor) / factor


async def get_suitable_provider(providers_list: list) -> str:
    # Get list of providers from Redis (sorted in ascending order of score)
    sorted_providers_list = await redis_db.zrange(name=settings.PROVIDERS_KEY, start=0, end=len(settings.PROVIDERS))

    # Remove from this list all providers that are not on `providers_list`
    sorted_providers_list = [row for row in sorted_providers_list if row in providers_list]

    provider = sorted_providers_list[0]

    return provider


async def clear_cache():
    cursor = -1
    while cursor != 0:
        cursor, keys = await redis_db.scan(cursor=cursor, match=f'{settings.CACHE_KEY_PREFIX}*', count=100)
        if keys:
            await redis_db.delete(*keys)
