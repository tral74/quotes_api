from typing import Union

import aiohttp
import aioredis

from app.config import settings


class AIOHTTPSession:
    """Wrapper for aiohttp.ClientSession"""

    _session: Union[aiohttp.ClientSession, None] = None

    def start(self):
        self._session = aiohttp.ClientSession()

    async def stop(self):
        if self._session:
            await self._session.close()
            self._session = None

    def __call__(self) -> aiohttp.ClientSession:
        if self._session is None:
            self.start()

        return self._session


redis_db = aioredis.from_url(url=settings.REDIS_URL, encoding="utf-8", decode_responses=True, max_connections=settings.REDIS_POOL_SIZE)

aiohttp_client = AIOHTTPSession()
