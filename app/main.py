import asyncio
import json
import ssl
import time
from copy import copy
from datetime import timedelta
from typing import Union

import uvicorn
from fastapi import FastAPI, HTTPException
from pydantic import PositiveInt
from starlette.responses import PlainTextResponse

from app.common import redis_db, aiohttp_client
from app.config import settings
from app.models import QuoteResponse, Currencies
from app.utils import truncate, get_suitable_provider, clear_cache

providers = settings.PROVIDERS

app = FastAPI(
    title="QuotesAPI",
    version="0.0.1",
)


@app.on_event("startup")
async def on_startup():
    aiohttp_client.start()

    # If cache is disabled
    if not settings.CACHE_ENABLED:
        # Clear cache
        await clear_cache()

    # Creating  initial provider dataset in Redis (for missing items)

    stored_providers = await redis_db.zrange(name=settings.PROVIDERS_KEY, start=0, end=len(settings.PROVIDERS))
    for provider in settings.PROVIDERS:
        if provider not in stored_providers:
            await redis_db.zadd(name=settings.PROVIDERS_KEY, mapping={provider: -1})


@app.on_event("shutdown")
async def on_shutdown():
    await aiohttp_client.stop()

    if redis_db:
        await redis_db.close()


@app.exception_handler(Exception)
async def exception_handler(request, exc):
    # TODO: logging
    return PlainTextResponse("Something went wrong", status_code=500)


async def get_rate_from_provider(provider_name: str, from_currency: str, to_currency: str) -> dict:
    result = {
        'provider': provider_name,
    }

    async with aiohttp_client().get(providers[provider_name]['url_template'] % from_currency, ssl=ssl.SSLContext()) as response:
        if response.status == 200:
            resp = await response.json()
            rate = resp.get('rates', {}).get(to_currency)
            if rate:
                result['exchange_rate'] = truncate(rate, settings.MAX_DECIMAL_PLACES_IN_RATE)

            # If `to_currency` is not found
            else:
                result['error'] = 'unsupported_to_currency'

        # If `from_currency` is not found
        elif response.status == 404:
            result['error'] = 'unsupported_from_currency'

    return result


async def get_exchange_rates(from_currency: str, to_currency: str) -> Union[dict, None]:
    result = None
    providers_list = None
    response_from_cache = False

    cache_key = f'{settings.CACHE_KEY_PREFIX}:{from_currency}:{to_currency}'

    # If caching is enabled
    if settings.CACHE_ENABLED:
        # Trying to get data from cache
        rate = await redis_db.get(name=cache_key)

        # If required rate is found in cache
        if rate:
            rate = json.loads(rate)
            if 'providers' in rate:
                rate['provider'] = await get_suitable_provider(providers_list=rate['providers'])
                del rate['providers']

            result = rate

            # Set a sign that the response is from the cache
            response_from_cache = True

    # No data found in cache, so requests to providers are needed
    if result is None:
        # Requesting data from providers
        tasks = [get_rate_from_provider(provider_name=p_name, from_currency=from_currency, to_currency=to_currency) for p_name in providers.keys()]
        rates = await asyncio.gather(*tasks)

        # Remove items without exchange rates
        rates = list(filter(lambda x: x.get('exchange_rate'), rates))

        # If there is data
        if rates:
            # Get max rate
            rate = max(map(lambda x: x.get('exchange_rate'), rates))

            # Get all providers with this rate
            providers_list = [row['provider'] for row in rates if row.get('exchange_rate') == rate]

            # If only one provider is found
            if len(providers_list) == 1:
                # Provider is first element
                provider = providers_list[0]

            # If multiple providers are found
            else:
                # Choose a provider according to the current balancing settings
                provider = await get_suitable_provider(providers_list)

            result = {
                'provider': provider,
                'rate': rate
            }

    # If there is data
    if result:
        # Save data for load balancer
        if settings.LOAD_BALANCER_MODE.value == 'requests_number':
            await redis_db.zincrby(name=settings.PROVIDERS_KEY, amount=1, value=result['provider'])

        elif settings.LOAD_BALANCER_MODE.value == 'last_request_time':
            await redis_db.zadd(name=settings.PROVIDERS_KEY, mapping={result['provider']: time.time_ns()})

        else:
            raise Exception('Invalid load balancer mode')

    # If the current data is not received from the cache and caching is enabled
    if not response_from_cache and settings.CACHE_ENABLED:
        # Caching
        cache_data = copy(result)
        if providers_list:
            cache_data['providers'] = providers_list

        await redis_db.setex(name=cache_key, time=timedelta(minutes=settings.TTL_IN_MINUTES), value=json.dumps(cache_data))

    return result


@app.get('/api/quote', response_model=QuoteResponse)
async def get_quote(from_currency_code: Currencies, amount: PositiveInt, to_currency_code: Currencies) -> QuoteResponse:
    # Get rates
    rate_data = await get_exchange_rates(from_currency=from_currency_code.value, to_currency=to_currency_code.value)
    if rate_data:
        rate = rate_data['rate']
        provider = rate_data['provider']

        return QuoteResponse(
            exchange_rate=rate,
            currency_code=to_currency_code,
            amount=amount * rate,
            provider_name=providers[provider]['title']
        )

    else:
        raise HTTPException(status_code=404, detail="Not found")


if __name__ == "__main__":
    uvicorn.run('main:app', host="127.0.0.1", port=8000, reload=True, workers=10)
