## Quotes API

Simple REST API for getting currency quotes from third party exchange service providers.

The API accepts requests from API clients requesting a quote to exchange money from one currency to another and returns the provider 
offering the best rate. For cases where multiple providers offer the same exchange rate, a simple load balancer is implemented.
Load balancer can work in two modes: by requests number or by last request time (see class **Settings** in the app/config.py)  

Data can be cached to reduce load and network traffic. Caching can be configured or disabled altogether (see **Settings** ).

The service is written in **Python** using **FastAPI** 

Data storage - **Redis**.

Redis is great for this service as a data store.
First, it allows you to implement data caching in just a few lines.
Second, the **sorted sets** makes it easy to implement the balancer.


**NOTE:**

If the service is running in a docker container and need to connect to Redis on the same machine, then  instead **localhost** or **127.0.0.1** use  **host.docker.internal**  

### Deployment

##### Build image

    docker build -t trall74/quote_api:v1 .

#####  Create container

    docker create --name quote_api -p 3000:8000 trall74/quote_api:v1

#####  Start container

    docker start quote_api

#####  Stop container

    docker stop quote_api


### OpenAPI documentation 

<BASE_URL>/docs
